const dark = document.querySelector(".dark-toggle");
const toChange = document.body;

dark.addEventListener('click', toggleDark)

function toggleDark() {
	toChange.classList.toggle("dark-mode");
};


$(document).ready(function () {

  $("body").mousemove(function (e) {
      handleMouseMove(e);
  });

  function handleMouseMove(event) {

      var x = event.pageX;
      var y = event.pageY;

      $("#orb").animate({
          left: x,
          top: y
      }, 1);
  }
});